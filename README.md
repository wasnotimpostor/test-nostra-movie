# Nostra Movie Sample Project



## About project :

This project is using Maven. First, you must install maven and jdk on your pc.
Use your database tools (ex : Sequel Pro on mac os) and create your own database with your own configuration (username and password).

After that, create new file under dir : src/main/resources named Application.properties and copy this [config](#application-properties) then put inside your Application.properties!

This project is using my team's S3 Bucket, but you are free to use it.

## How to check the API's

After this project running successfully, you can open your localhost (with your port) on the browser and put /swagger-ui after localhost.

Example :
```
localhost:8080/swagger-ui
```

## Error Flyway Migration

If you find any error about flyway migration, you can comment this [flyway dependency](#flyway-dependency) on pom.xml then put this config inside your Application.properties
```
spring.jpa.hibernate.ddl-auto = update
```

***

# Application Properties

```
spring.datasource.url =jdbc:mysql://localhost:[YOUR_DB_PORT]/[YOUR_DB_NAME]?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Asia/Jakarta
server.port=[PORT, ex : 8080]
springdoc.swagger-ui.path=/swagger-ui

# Username and password
spring.datasource.username = [YOUR_DB_USERNAME]
spring.datasource.password = [YOUR_DB_PASSWORD]
spring.datasource.driver-class-name= com.mysql.cj.jdbc.Driver
spring.jpa.hibernate.naming.physical-strategy=org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl

# Jwt Config
nostra.movie.secretkey=@testNostraMovie_2022
nostra.movie.jwtexpiration=8640000

# S3 Config
nostra.movie.accessKey=AKIAW3DJKF2IJIWD44BH
nostra.movie.secretKey=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
nostra.movie.region=ap-southeast-1
nostra.movie.bucket-name=hbs-sby
nostra.movie.bucket-url=https://s3.ap-southeast-1.amazonaws.com
```

# Flyway Dependency

```
<dependency>
    <groupId>org.flywaydb</groupId>
    <artifactId>flyway-core</artifactId>
    <version>7.15.0</version>
</dependency>
```
