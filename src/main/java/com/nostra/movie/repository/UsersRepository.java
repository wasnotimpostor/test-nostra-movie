package com.nostra.movie.repository;

import com.nostra.movie.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Integer> {
    @Query(
            value =
                    "select u.*\n"
                            + "    from users u\n"
                            + "    where (u.username = binary(?1)\n"
                            + "    or u.email = binary(?1))",
            nativeQuery = true)
    Optional<Users> findByUsernameOrEmail(String username);

    Optional<Users> findByEmail(String email);

    Boolean existsByEmail(String email);
}
