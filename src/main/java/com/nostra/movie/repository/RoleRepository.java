package com.nostra.movie.repository;

import com.nostra.movie.entity.Roles;
import com.nostra.movie.entity.enumerated.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Roles, Integer> {
    Optional<Roles> findByName(RoleName roleName);
}
