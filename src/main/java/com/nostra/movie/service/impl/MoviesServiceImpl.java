package com.nostra.movie.service.impl;

import com.nostra.movie.config.util.CustomException;
import com.nostra.movie.config.util.paging.CustomPage;
import com.nostra.movie.config.util.paging.PageUtil;
import com.nostra.movie.dto.response.DtoMoviesByPage;
import com.nostra.movie.entity.Movies;
import com.nostra.movie.repository.MoviesRepository;
import com.nostra.movie.service.MoviesService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MoviesServiceImpl implements MoviesService {

    @Autowired private MoviesRepository moviesRepository;
    private final JdbcTemplate jdbcTemplate;

    @Override
    public Movies save(Movies movies) {
        return moviesRepository.save(movies);
    }

    @Override
    public Movies delete(Integer id) throws Exception{
        var movie = this.findById(id);
        moviesRepository.deleteById(id);
        return movie;
    }

    @Override
    public Movies findById(Integer id) throws Exception{
        var movie = moviesRepository.findById(id);
        if (!movie.isPresent())
            throw new CustomException("Data not found.", HttpStatus.NOT_FOUND);
        return movie.get();
    }

    @Override
    public CustomPage<DtoMoviesByPage> page(String title, Pageable pageable) {
        Object[] param = {title+"%"};
        String q = "select m.id as id, m.title as title, m.year as year, group_concat(g.name separator ', ') as genres, m.director as director, m.actors as actors, m.plot as plot, m.posterUrl as posterUrl "+
                "from movies as m "+
                "inner join movie_genres as mg on m.id = mg.movie_id "+
                "inner join genres as g on mg.genre_id = g.id "+
                "where m.title like ? ";
        String g = "group by m.id, m.title, m.year, m.director, m.actors, m.plot, m.posterUrl";
        String c = "select count(distinct(m.id)) from movies as m "+
                "inner join movie_genres as mg on m.id = mg.movie_id "+
                "inner join genres as g on mg.genre_id = g.id "+
                "where m.title like ? ";

        var totalRow = PageUtil.getTotalRow(jdbcTemplate, param, c);
        var data = jdbcTemplate.query(
                PageUtil.createPageable(q+g, pageable),
                (rsp, rowNum) ->
                        DtoMoviesByPage.builder()
                                .id(rsp.getInt("id"))
                                .title(rsp.getString("title"))
                                .year(rsp.getInt("year"))
                                .genres(rsp.getString("genres"))
                                .director(rsp.getString("director"))
                                .actors(rsp.getString("actors"))
                                .plot(rsp.getString("plot"))
                                .posterUrl(rsp.getString("posterUrl"))
                                .build(),
                param);
        var page = new PageImpl<>(data, pageable, totalRow);
        return new CustomPage<>(
                page.getTotalElements(),
                page.getTotalPages(),
                page.getSize(),
                pageable.getPageNumber(),
                data
        );
    }
}
