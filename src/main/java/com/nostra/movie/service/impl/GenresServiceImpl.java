package com.nostra.movie.service.impl;

import com.nostra.movie.config.util.CustomException;
import com.nostra.movie.entity.Genres;
import com.nostra.movie.repository.GenresRepository;
import com.nostra.movie.service.GenresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GenresServiceImpl implements GenresService {

    @Autowired private GenresRepository genresRepository;

    @Override
    public Genres save(Genres genres) {
        return genresRepository.save(genres);
    }

    @Override
    @Transactional(rollbackFor = {
            Exception.class,
            RuntimeException.class
    })
    public List<Genres> batchCreate(List<Genres> genres) {
        return genresRepository.saveAll(genres);
    }

    @Override
    public Genres findById(Integer id) throws Exception {
        var exist = genresRepository.findById(id);
        if (!exist.isPresent())
            throw new CustomException("Data not found.", HttpStatus.NOT_FOUND);
        return exist.get();
    }

    @Override
    public Genres delete(Integer id) throws Exception {
        var genres = this.findById(id);
        genresRepository.deleteById(id);
        return genres;
    }

    @Override
    public List<Genres> list() {
        return genresRepository.findAll();
    }
}
