package com.nostra.movie.service.impl;

import com.nostra.movie.config.util.CustomException;
import com.nostra.movie.dto.response.DtoUserById;
import com.nostra.movie.entity.Users;
import com.nostra.movie.repository.UsersRepository;
import com.nostra.movie.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired private UsersRepository usersRepository;

    @Override
    public Users save(Users users) {
        return usersRepository.save(users);
    }

    @Override
    public DtoUserById findById(Integer user_id) throws Exception {
        var user = usersRepository.findById(user_id);
        if (!user.isPresent())
            throw new CustomException("User not found.", HttpStatus.NOT_FOUND);
        DtoUserById dtoUserById = DtoUserById.builder()
                .id(user_id)
                .email(user.get().getEmail())
                .first_name(user.get().getFirst_name())
                .last_name(user.get().getLast_name())
                .role(user.get().getRoles())
                .build();
        return dtoUserById;
    }

    @Override
    public Optional<Users> getByUsernameOrEmail(String usernameOrEmail) {
        var user = usersRepository.findByUsernameOrEmail(usernameOrEmail);
        return user;
    }

    @Override
    public Optional<Users> delete(Integer user_id) throws Exception {
        var user = usersRepository.findById(user_id);
        if (!user.isPresent())
            throw new CustomException("User not found.", HttpStatus.NOT_FOUND);
        usersRepository.deleteById(user_id);
        return user;
    }

    @Override
    public Boolean existsByEmail(String email) {
        return usersRepository.existsByEmail(email);
    }

    @Override
    public Users getUser(Integer id) throws Exception{
        var user = usersRepository.findById(id);
        if (!user.isPresent())
            throw new CustomException("User not found.", HttpStatus.NOT_FOUND);
        return user.get();
    }
}
