package com.nostra.movie.service.impl;

import com.nostra.movie.entity.Roles;
import com.nostra.movie.entity.enumerated.RoleName;
import com.nostra.movie.repository.RoleRepository;
import com.nostra.movie.service.RolesService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RolesServiceImpl implements RolesService {

    @Autowired private RoleRepository repository;
    private final JdbcTemplate jdbcTemplate;

    @Override
    public Optional<Roles> findByName(RoleName roleName) {
        return repository.findByName(roleName);
    }

    @Override
    public Object getAllRoles(String role) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (role.equals(RoleName.Admin.toString())){
            String q = "select * from roles where id > 0";
            list = jdbcTemplate.queryForList(q);
        }
        if (role.equals(RoleName.User.toString())){
            String q = "select * from roles where name = '"+RoleName.User+"'";
            list = jdbcTemplate.queryForList(q);
        }
        return list;
    }
}
