package com.nostra.movie.service;

import com.nostra.movie.config.util.paging.CustomPage;
import com.nostra.movie.dto.response.DtoMoviesByPage;
import com.nostra.movie.entity.Movies;
import org.springframework.data.domain.Pageable;

public interface MoviesService {

    Movies save(Movies movies);
    Movies delete(Integer id) throws Exception;
    Movies findById(Integer id) throws Exception;
    CustomPage<DtoMoviesByPage> page(String title, Pageable pageable);
}
