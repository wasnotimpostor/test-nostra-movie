package com.nostra.movie.service;

import com.nostra.movie.entity.Roles;
import com.nostra.movie.entity.enumerated.RoleName;

import java.util.Optional;

public interface RolesService {
    Optional<Roles> findByName(RoleName roleName);
    Object getAllRoles(String role);
}
