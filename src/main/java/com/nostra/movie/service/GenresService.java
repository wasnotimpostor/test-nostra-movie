package com.nostra.movie.service;

import com.nostra.movie.entity.Genres;

import java.util.List;

public interface GenresService {
    Genres save(Genres genres);
    List<Genres> batchCreate(List<Genres> genres);
    Genres findById(Integer id) throws Exception;
    Genres delete(Integer id) throws Exception;
    List<Genres> list();
}
