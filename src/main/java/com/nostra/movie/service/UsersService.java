package com.nostra.movie.service;

import com.nostra.movie.dto.response.DtoUserById;
import com.nostra.movie.entity.Users;

import java.util.Optional;

public interface UsersService {
    Users save(Users users);
    DtoUserById findById(Integer user_id) throws Exception;
    Optional<Users> getByUsernameOrEmail(String usernameOrEmail);
    Optional<Users> delete(Integer user_id) throws Exception;
    Boolean existsByEmail(String email);
    Users getUser(Integer id) throws Exception;
}
