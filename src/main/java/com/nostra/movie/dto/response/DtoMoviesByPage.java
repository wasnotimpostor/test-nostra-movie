package com.nostra.movie.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DtoMoviesByPage {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("year")
    private Integer year;

    @JsonProperty("genres")
    private String genres;

    @JsonProperty("director")
    private String director;

    @JsonProperty("actors")
    private String actors;

    @JsonProperty("plot")
    private String plot;

    @JsonProperty("posterUrl")
    private String posterUrl;
}
