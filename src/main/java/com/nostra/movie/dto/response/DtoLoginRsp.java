package com.nostra.movie.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DtoLoginRsp {

    @JsonProperty("token")
    private String token;
}
