package com.nostra.movie.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nostra.movie.entity.Roles;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class DtoUserById {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("email")
    private String email;

    @JsonProperty("firstName")
    private String first_name;

    @JsonProperty("lastName")
    private String last_name;

    @JsonProperty("role")
    private Set<Roles> role;
}
