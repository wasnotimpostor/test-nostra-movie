package com.nostra.movie.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DtoRegisterUser {

    @NotNull(message = "email is required")
    @JsonProperty("email")
    private String email;

    @NotNull(message = "username is required")
    @JsonProperty("username")
    private String username;

    @NotNull(message = "password is required")
    @JsonProperty("password")
    private String password;

    @NotNull(message = "first_name is required")
    @JsonProperty("first_name")
    private String first_name;

    @JsonProperty("last_name")
    private String last_name;

    @JsonProperty("phone_number")
    private String phone_number;

    @NotNull(message = "roleId is required")
    @JsonProperty("roleId")
    private Set<Integer> role_id;
}
