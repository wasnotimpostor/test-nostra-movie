package com.nostra.movie.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DtoLoginReq {

    @NotNull(message = "username or email is required")
    @JsonProperty("usernameOrEmail")
    private String usernameOrEmail;

    @NotNull(message = "password is required")
    @JsonProperty("password")
    private String password;
}
