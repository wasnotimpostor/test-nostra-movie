package com.nostra.movie.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DtoMovieReq {

    @NotNull(message = "title is required")
    @JsonProperty("title")
    private String title;

    @JsonProperty("year")
    private Integer year;

    @JsonProperty("runtime")
    private String runtime;

    @NotNull(message = "genres_id is required")
    @JsonProperty("genres_id")
    private List<Integer> genres_id;

    @JsonProperty("director")
    private String director;

    @JsonProperty("actors")
    private String actors;

    @JsonProperty("plot")
    private String plot;

//    @NotNull
    @JsonProperty("posterUrl")
    private String posterUrl;
}
