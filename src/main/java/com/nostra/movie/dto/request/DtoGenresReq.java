package com.nostra.movie.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DtoGenresReq {

    @NotNull(message = "name is required")
    @JsonProperty("name")
    private String name;
}
