package com.nostra.movie.controller;

import com.nostra.movie.config.security.JwtProvider;
import com.nostra.movie.config.util.CustomException;
import com.nostra.movie.config.util.GlobalApiResponse;
import com.nostra.movie.dto.request.DtoUpdateUser;
import com.nostra.movie.service.RolesService;
import com.nostra.movie.service.UsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class UserRoleController {

    @Autowired private UsersService usersService;
    @Autowired private RolesService rolesService;
    @Autowired private JwtProvider jwtProvider;

    void preExecute(HttpServletRequest request) throws Exception {
        if (request.getHeader("Authorization") == null){
            throw new CustomException("Token is required", HttpStatus.BAD_REQUEST);
        }
        if (jwtProvider.isExpired(request.getHeader("Authorization"))){
            throw new CustomException("Token is required", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "roles", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> getRoles(
            HttpServletRequest request
    ) throws Exception {
        this.preExecute(request);
        var role = jwtProvider.getRoleFromToken(request.getHeader("Authorization").replace("Bearer ", ""));
        role = role.replace("authority=","");
        role = role.substring(2, role.length()-2);
        return new GlobalApiResponse<>(HttpStatus.OK.value(), rolesService.getAllRoles(role));
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> getById(
            @PathVariable(name = "id") Integer id
    ) throws Exception {
        return new GlobalApiResponse<>(HttpStatus.OK.value(), usersService.findById(id));
    }

    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> update(
            @PathVariable(name = "id") Integer id,
            @RequestBody @Valid DtoUpdateUser req
    ) throws Exception {
        var user = usersService.getUser(id);
        user.setFirst_name(req.getFirst_name());
        user.setLast_name(req.getLast_name());
        user.setPhone_number(req.getPhone_number());
        var save = usersService.save(user);
        return new GlobalApiResponse<>(HttpStatus.OK.value(), save);
    }
}
