package com.nostra.movie.controller;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.nostra.movie.config.util.CustomException;
import com.nostra.movie.config.util.GlobalApiResponse;
import com.nostra.movie.config.util.S3StorageService;
import com.nostra.movie.dto.request.DtoMovieReq;
import com.nostra.movie.entity.Genres;
import com.nostra.movie.entity.Movies;
import com.nostra.movie.service.GenresService;
import com.nostra.movie.service.MoviesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/movies", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class MoviesController {

    @Autowired private MoviesService moviesService;
    @Autowired private GenresService genresService;
    @Autowired private S3StorageService s3StorageService;

    public List<Genres> listGenres(List<Integer> genres_id) throws Exception {
        List<Genres> genres = new ArrayList<>();
        for (int i=0; i<genres_id.size(); i++){
            var getOne = genresService.findById(genres_id.get(i));
            genres.add(i, getOne);
        }
        return genres;
    }

    public Movies build(Integer id, String title, Integer year, String runtime, List<Genres> genres,
                        String director, String actors, String plot, String posterUrl) throws Exception{
        var exist = moviesService.findById(id);
        if (exist == null){
            Movies movies = Movies.builder()
                    .title(title)
                    .year(year)
                    .runtime(runtime)
                    .genres(genres)
                    .director(director)
                    .actors(actors)
                    .plot(plot)
                    .posterUrl(posterUrl)
                    .build();
            return movies;
        } else {
            exist.setTitle(title);
            exist.setYear(year);
            exist.setRuntime(runtime);
            exist.setGenres(genres);
            exist.setDirector(director);
            exist.setActors(actors);
            exist.setPlot(plot);
            exist.setPosterUrl(posterUrl);
            return exist;
        }
    }

    @Transactional(rollbackFor = {
            Exception.class,
            RuntimeException.class,
            AmazonServiceException.class,
            AmazonClientException.class,
            CustomException.class
    })
    @PostMapping(value = "create", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public GlobalApiResponse<?> create(
            @RequestParam String title,
            @RequestParam(required = false) Integer year,
            @RequestParam(required = false) String runtime,
            @RequestParam List<Integer> genres_id,
            @RequestParam(required = false) String director,
            @RequestParam(required = false) String actors,
            @RequestParam(required = false) String plot,
            @RequestParam MultipartFile file
    ) throws Exception {
        var posterUrl = s3StorageService.upload(file, "nostra");
        var genres = this.listGenres(genres_id);
        Movies movies = this.build(0, title, year, runtime, genres, director, actors, plot, posterUrl);
        return new GlobalApiResponse<>(HttpStatus.OK.value(), moviesService.save(movies));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> pageable(
            @RequestParam(required = false, defaultValue = "") String title,
            Pageable pageable
    ){
        return new GlobalApiResponse<>(HttpStatus.OK.value(), moviesService.page(title, pageable));
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> getById(
            @PathVariable(name = "id") Integer id
    ) throws Exception {
        return new GlobalApiResponse<>(HttpStatus.OK.value(), moviesService.findById(id));
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> delete(
            @PathVariable(name = "id") Integer id
    ) throws Exception {
        var movie = moviesService.findById(id);
        s3StorageService.delete(movie.getPosterUrl(), "nostra");
        return new GlobalApiResponse<>(HttpStatus.OK.value(), moviesService.delete(id));
    }

    @Transactional(rollbackFor = {
            Exception.class,
            RuntimeException.class,
            AmazonServiceException.class,
            AmazonClientException.class,
            CustomException.class
    })
    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public GlobalApiResponse<?> update(
            @PathVariable(name = "id") Integer id,
            @RequestParam String title,
            @RequestParam(required = false) Integer year,
            @RequestParam(required = false) String runtime,
            @RequestParam List<Integer> genres_id,
            @RequestParam(required = false) String director,
            @RequestParam(required = false) String actors,
            @RequestParam(required = false) String plot,
            @RequestParam(required = false) MultipartFile file
    ) throws Exception {
        var movie = moviesService.findById(id);
        s3StorageService.delete(movie.getPosterUrl(), "nostra");
        var posterUrl = s3StorageService.upload(file, "nostra");
        var genres = this.listGenres(genres_id);
        Movies movies = this.build(id, title, year, runtime, genres, director, actors, plot, posterUrl);
        return new GlobalApiResponse<>(HttpStatus.OK.value(), moviesService.save(movies));
    }
}
