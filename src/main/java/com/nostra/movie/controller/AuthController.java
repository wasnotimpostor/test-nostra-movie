package com.nostra.movie.controller;

import com.nostra.movie.config.security.JwtProvider;
import com.nostra.movie.config.util.CustomException;
import com.nostra.movie.config.util.GlobalApiResponse;
import com.nostra.movie.dto.request.DtoLoginReq;
import com.nostra.movie.dto.request.DtoRegisterUser;
import com.nostra.movie.dto.response.DtoLoginRsp;
import com.nostra.movie.entity.Roles;
import com.nostra.movie.entity.Users;
import com.nostra.movie.entity.enumerated.RoleName;
import com.nostra.movie.service.RolesService;
import com.nostra.movie.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {

    @Autowired private UsersService usersService;
    @Autowired private RolesService rolesService;
    @Autowired private JwtProvider jwtProvider;
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @PostMapping(value = "login", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> login(
            @RequestBody @Valid DtoLoginReq req
    ) throws Exception {
        var user = usersService.getByUsernameOrEmail(req.getUsernameOrEmail());
        if (!user.isPresent())
            throw new CustomException("User not found.", HttpStatus.NOT_FOUND);
        if (!bCryptPasswordEncoder.matches(req.getPassword(), user.get().getPassword()))
            throw new CustomException("Password did not match.", HttpStatus.BAD_REQUEST);
        var token = jwtProvider.generateToken(user.get());
        DtoLoginRsp rsp = DtoLoginRsp.builder().token(token).build();
        return new GlobalApiResponse<>(HttpStatus.OK.value(), rsp);
    }

    @PostMapping(value = "register", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> register(
            @RequestBody @Valid DtoRegisterUser registerUser
    ) throws Exception{
        var email_exist = usersService.getByUsernameOrEmail(registerUser.getEmail());
        if (email_exist.isPresent())
            throw new CustomException("Email sudah terdaftar.", HttpStatus.CONFLICT);
        var username_exist = usersService.getByUsernameOrEmail(registerUser.getUsername());
        if (username_exist.isPresent())
            throw new CustomException("Username sudah terdaftar.", HttpStatus.CONFLICT);

        // build user
        Users users = Users.builder()
                .username(registerUser.getUsername())
                .email(registerUser.getEmail())
                .first_name(registerUser.getFirst_name())
                .last_name(registerUser.getLast_name())
                .password(new BCryptPasswordEncoder().encode(registerUser.getPassword()))
                .phone_number(registerUser.getPhone_number())
                .build();

        // set roles
        Set<Integer> strRoles = registerUser.getRole_id();
        Set<Roles> roles = new HashSet<>();
        strRoles.forEach(role -> {
            switch (role) {
                case 1:
                    var admin = rolesService.findByName(RoleName.Admin);
                    roles.add(admin.get());
                    break;
                case 2:
                    var user = rolesService.findByName(RoleName.User);
                    roles.add(user.get());
                    break;
            }
        });
        users.setRoles(roles);
        var save = usersService.save(users);
        return new GlobalApiResponse<>(HttpStatus.OK.value(), save);
    }
}
