package com.nostra.movie.controller;

import com.nostra.movie.config.util.GlobalApiResponse;
import com.nostra.movie.dto.request.DtoGenresReq;
import com.nostra.movie.entity.Genres;
import com.nostra.movie.service.GenresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/genres", produces = MediaType.APPLICATION_JSON_VALUE)
public class GenresController {

    @Autowired private GenresService genresService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> list(){
        return new GlobalApiResponse<>(HttpStatus.OK.value(), genresService.list());
    }

    @PostMapping(value = "create", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> create(
            @RequestBody @Valid DtoGenresReq req
    ){
        Genres genres = Genres.builder().name(req.getName()).build();
        return new GlobalApiResponse<>(HttpStatus.OK.value(), genresService.save(genres));
    }

    @PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> update(
            @PathVariable(name = "id") Integer id,
            @RequestBody @Valid DtoGenresReq req
    ) throws Exception {
        var genres = genresService.findById(id);
        genres.setName(req.getName());
        return new GlobalApiResponse<>(HttpStatus.OK.value(), genresService.save(genres));
    }

    @PostMapping(value = "create:batch", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> batchCreate(
            @RequestBody @Valid List<DtoGenresReq> req
    ){
        List<Genres> list = new ArrayList<>();
        for (int i=0; i<req.size(); i++){
            Genres genres = Genres.builder().name(req.get(i).getName()).build();
            list.add(genres);
        }
        return new GlobalApiResponse<>(HttpStatus.OK.value(), genresService.batchCreate(list));
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> getById(
            @PathVariable(name = "id") Integer id
    ) throws Exception {
        return new GlobalApiResponse<>(HttpStatus.OK.value(), genresService.findById(id));
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> delete(
            @PathVariable(name = "id") Integer id
    ) throws Exception {
        return new GlobalApiResponse<>(HttpStatus.OK.value(), genresService.delete(id));
    }
}
