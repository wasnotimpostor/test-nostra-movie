package com.nostra.movie.config.util.paging;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CustomPage<T>{
    @JsonProperty("total_item")
    private long totalItem;

    @JsonProperty("total_page")
    private int totalPage;

    @JsonProperty("per_page")
    private int perPage;

    @JsonProperty("current_page")
    private int currentPage;

    private List<T> data;
}
