package com.nostra.movie.config.util.paging;

import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;

public class PageUtil {

    public static String createPageable(String sqlQuery, Pageable pageable){
        return sqlQuery + " LIMIT " + pageable.getPageSize() + " OFFSET " + pageable.getOffset();
    }

    public static long getTotalRow(JdbcTemplate jdbcTemplate, Object[] param, String sql) {
        var totalRow = jdbcTemplate.queryForObject(sql, Integer.class, param);
        return totalRow == null ? 0 : totalRow;
    }
}
