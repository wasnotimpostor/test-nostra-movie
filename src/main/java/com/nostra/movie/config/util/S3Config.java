package com.nostra.movie.config.util;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3Config {
    @Value("${nostra.movie.accessKey}")
    private String awsId;

    @Value("${nostra.movie.secretKey}")
    private String awsKey;

    @Value("${nostra.movie.region}")
    private String region;

    @Bean
    public AmazonS3 s3(){
        AWSCredentials awsCredentials =  new BasicAWSCredentials(awsId, awsKey);
        AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.fromName(region))
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
        return amazonS3;
    }
}
