package com.nostra.movie.config.util;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.databasemigrationservice.model.S3ResourceNotFoundException;
import com.amazonaws.services.mediastoredata.model.ObjectNotFoundException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Slf4j
public class S3StorageService {

    @Autowired
    private AmazonS3 amazonS3;

    @Value("${nostra.movie.bucket-name}")
    private String bucketName;

    @Value("${nostra.movie.bucket-url}")
    private String url;

    public String generateFileName(MultipartFile multipartFile){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
        String newDate = sdf.format(date);
        String fileName = newDate  + "-" + multipartFile.getOriginalFilename().replace(" ", "_");
        return fileName;
    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public String upload(MultipartFile file, String bucket){
        String fileUrl = "";

        try {
            File conv = convertMultiPartToFile(file);
            String fileName = generateFileName(file);
            amazonS3.putObject(new PutObjectRequest(bucketName+"/"+bucket, fileName, conv).withCannedAcl(CannedAccessControlList.PublicRead));
            fileUrl = url + "/" + bucketName+"/"+bucket + "/" + fileName;
            conv.delete();
            return fileUrl;
        } catch (IOException ioe){
            log.error("IOException: {}" + ioe.getMessage());
        } catch (AmazonServiceException serviceException) {
            log.error("AmazonServiceException: {}"+ serviceException.getMessage());
            throw serviceException;
        } catch (AmazonClientException clientException) {
            log.error("AmazonClientException Message: {}" + clientException.getMessage());
            throw clientException;
        }
        return fileUrl;
    }

    public String delete(String url, String bucket){
        String fileName = "";
        try {
            String separatedFromBucket = url.substring(0, 49+bucket.length());
            fileName = url.substring(separatedFromBucket.length());
            amazonS3.deleteObject(bucketName+"/"+bucket, fileName);
        } catch (S3ResourceNotFoundException e){
            log.error("error S3 => {}",  e.getMessage());
        } catch (ObjectNotFoundException e){
            log.error("error S3 => {}", e.getMessage());
        }
        return fileName+" deleted successfully";
    }
}
