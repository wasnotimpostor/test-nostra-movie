package com.nostra.movie.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nostra.movie.entity.enumerated.RoleName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "roles")
public class Roles {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer Id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(name = "role_name")
    private RoleName name;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    private List<Users> userList = new ArrayList<>();
}
