package com.nostra.movie.entity.enumerated;

public enum RoleName {
    Admin,
    User
}
