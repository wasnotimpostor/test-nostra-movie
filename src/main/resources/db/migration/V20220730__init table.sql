CREATE TABLE IF NOT EXISTS `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `created_at` datetime DEFAULT NULL,
    `email` varchar(255) NOT NULL,
    `first_name` varchar(255) NOT NULL,
    `last_name` varchar(255) DEFAULT NULL,
    `password` varchar(255) DEFAULT NULL,
    `phone_number` varchar(255) DEFAULT NULL,
    `updated_at` datetime DEFAULT NULL,
    `username` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`),
    UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_roles` (
    `user_id` int(11) NOT NULL,
    `role_id` int(11) NOT NULL,
    PRIMARY KEY (`user_id`,`role_id`),
    KEY `role_id` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `roles` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `role_name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `role_name` (`role_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `movies` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `actors` varchar(255) DEFAULT NULL,
    `created_at` datetime DEFAULT NULL,
    `director` varchar(255) DEFAULT NULL,
    `plot` varchar(255) DEFAULT NULL,
    `posterUrl` varchar(255) NOT NULL,
    `runtime` varchar(255) DEFAULT NULL,
    `title` varchar(255) NOT NULL,
    `updated_at` datetime DEFAULT NULL,
    `year` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `movie_genres` (
    `movie_id` int(11) NOT NULL,
    `genre_id` int(11) NOT NULL,
    KEY `genre_id` (`genre_id`),
    KEY `movie_id` (`movie_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `genres` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `created_at` datetime DEFAULT NULL,
    `name` varchar(255) NOT NULL,
    `updated_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;