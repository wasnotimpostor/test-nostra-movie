INSERT INTO `roles` (`id`, `role_name`)
VALUES
(1, 'Admin'),
(2, 'User');

INSERT INTO `users` (`id`, `created_at`, `email`, `first_name`, `last_name`, `password`, `phone_number`, `updated_at`, `username`)
VALUES
(1, current_timestamp, 'admin@mail.com', 'admin', 'admin', '$2a$10$97tAzpB6z6uNGGC1.sa2cO1OOYS2h0VG0HlcOY3zBtjEmo7UQCjLG', '0822', current_timestamp, 'admin');

INSERT INTO `user_roles` (`user_id`, `role_id`)
VALUES
(1, 1);

INSERT INTO `genres` (`id`, `created_at`, `name`, `updated_at`)
VALUES
(1, current_timestamp, 'Horror', current_timestamp),
(2, current_timestamp, 'Thriller', current_timestamp),
(3, current_timestamp, 'Fantasy', current_timestamp),
(4, current_timestamp, 'Sci-Fi', current_timestamp),
(5, current_timestamp, 'Action', current_timestamp);
