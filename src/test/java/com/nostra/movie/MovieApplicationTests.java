package com.nostra.movie;

import com.nostra.movie.config.security.JwtProvider;
import com.nostra.movie.service.RolesService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(
		classes = MovieApplication.class,
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
class MovieApplicationTests {

	@Autowired private JwtProvider jwtProvider;
	@Autowired private RolesService rolesService;
	String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3IiwiZmlyc3RfbmFtZSI6InN0cmluZyIsImxhc3RfbmFtZSI6InN0cmluZyIsInJvbGUiOlt7ImF1dGhvcml0eSI6IkFkbWluIn1dLCJlbWFpbCI6ImVwYW5AZ21haWwuY29tIiwiaWF0IjoxNjU5MjU0NTcwLCJleHAiOjE2NTkyNjMyMTB9.sMvPdIK2Mus5bE_54bJk_IbfnkYBmyoe91UkHb4THzc";

	@Test
	void checkRoleFromToken() {
		var role = jwtProvider.getRoleFromToken(token);
		role = role.replace("authority=","");
		role = role.substring(2, role.length()-2);
		System.out.println(role);
	}

	@Test
	void getRoles(){
		var role = jwtProvider.getRoleFromToken(token);
		role = role.replace("authority=","");
		role = role.substring(2, role.length()-2);
		System.out.println(rolesService.getAllRoles(role));
	}
}
